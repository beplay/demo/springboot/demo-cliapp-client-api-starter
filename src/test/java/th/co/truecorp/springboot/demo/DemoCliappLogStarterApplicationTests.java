package th.co.truecorp.springboot.demo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import lombok.extern.slf4j.Slf4j;
import th.co.truecorp.client.demo.JaxbConfig;
import th.co.truecorp.client.demo.SimpleRestTemplateService;
import th.co.truecorp.client.demo.dto.ListUsersResponse;
import th.co.truecorp.springboot.web.client.HttpClientConfig;
import th.co.truecorp.springboot.web.client.HttpClientConfigurationResolver;
import th.co.truecorp.springboot.web.client.RestTemplateConfig;
import th.co.truecorp.springboot.web.client.model.HttpClientEndpoint;

@Slf4j
@SpringBootTest(classes = {
    SimpleRestTemplateService.class
    , RestTemplateConfig.class
    , HttpClientConfig.class
    , HttpClientConfigurationResolver.class
    , JaxbConfig.class
    , HttpClientEndpoint.class
})
class DemoCliappLogStarterApplicationTests {
  

  @Autowired
  private SimpleRestTemplateService simpleRst;

	@Test
	void contextLoads() {
    System.out.println("TEst");
    
  log.info("Hello, starting app..");
    
    ListUsersResponse  usersResponse = null;
    
    usersResponse = simpleRst.getUsers();
    
    log.info("Found {} users. Page {} of {}" 
      , usersResponse.getUsers().size()
      , usersResponse.getPage()
      , usersResponse.getTotalPages()
    );
	}

}