package th.co.truecorp.client.demo;

import java.util.Arrays;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import th.co.truecorp.client.demo.dto.ListUsersResponse;
import th.co.truecorp.springboot.web.client.annotation.HttpBasicAuthen;
import th.co.truecorp.springboot.web.client.annotation.HttpEndpoint;

/**
 * Example test with https://reqres.in/
 * 
 * 
 * @author chitchai
 *
 */
@Service
public class SimpleRestTemplateService {

	/**
	 * This object will not found configuration then use hardcode url
	 */
	@HttpEndpoint(name="users-static", url="https://reqres.in/api/users")
	@HttpBasicAuthen(username="xx", password="yy")
	private RestTemplate restStatic;
	
	/**
	 * This object will use configuration from application.yaml
	 * Configuration name => http.client.endpoints.users-api
	 */
	@HttpEndpoint(name="users-api")
	@HttpBasicAuthen(username="xx", password="yy")
	private RestTemplate restApi;
	
	
	
	/**
	 * Get all users
	 * @return
	 */
	public ListUsersResponse  getUsers() {
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		
//		ResponseEntity<ListUsersResponse> response = restStatic.exchange(
		ResponseEntity<ListUsersResponse> response = restApi.exchange(
			 UriComponentsBuilder.newInstance()
//			 	.path("/")
			 	.toUriString()
			, HttpMethod.GET
//			, new HttpEntity<Map<String, Object>>(null, headers)
			, new HttpEntity<String>("")
			, ListUsersResponse.class
			);
		
		return response.getBody();
	}
	
	

}
