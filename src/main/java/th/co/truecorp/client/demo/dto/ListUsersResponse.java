package th.co.truecorp.client.demo.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import lombok.Data;
import th.co.truecorp.client.demo.model.User;

@Data
@XmlAccessorType(XmlAccessType.NONE)
public class ListUsersResponse {

	@XmlElement(name="page")
	private int page;
	
	@XmlElement(name="total_pages")
	private int totalPages;
	
	@XmlElement(name="data")
	private List<User>  users;
}
