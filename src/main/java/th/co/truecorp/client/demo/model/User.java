package th.co.truecorp.client.demo.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import lombok.Data;

@XmlAccessorType(XmlAccessType.NONE)
@Data
public class User {

	@XmlElement(name="id")
	private int id;
	
	@XmlElement(name="first_name")
	private String firstName;
	
	@XmlElement(name="last_name")
	private String lastName;
	
	@XmlElement(name="email")
	private String email;
	
}
