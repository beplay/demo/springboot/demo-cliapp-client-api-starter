package th.co.truecorp.client.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import lombok.extern.slf4j.Slf4j;
import th.co.truecorp.client.demo.dto.ListUsersResponse;

@Slf4j
@SpringBootApplication
public class DemoClientApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(DemoClientApplication.class, args);
	}

	@Autowired
	private SimpleRestTemplateService simpleRst;
	
	@Override
	public void run(String... args) throws Exception {
		log.info("Hello, starting app..");
		
		ListUsersResponse  usersResponse = null;
		
		usersResponse = simpleRst.getUsers();
		
		log.info("Found {} users. Page {} of {}" 
			, usersResponse.getUsers().size()
			, usersResponse.getPage()
			, usersResponse.getTotalPages()
		);
		
		System.exit(SpringApplication.exit(context));
		
	}
	
	
	@Autowired
  private ConfigurableApplicationContext context;


}
