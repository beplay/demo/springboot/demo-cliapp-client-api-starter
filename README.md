# Demo client API starter

This demo will be test with [Online dummy API](https://reqres.in/)



























This example will generate Json log. 

## Usecase 1: Generate log file to `logs` directory

```bash
gradle bootrun
```

Log file will be generate as follow

```sh
logs
├── app.json.log
├── app.json.log.20200228
├── app.log
├── http.json.log
└── springboot.log

```
---------------------

## Usecase 2: Generate log to console ( for Fluend K8S )

```bash

export KUBERNETES_SERVICE_HOST=localhost

gradle bootrun
```

Out Json log file will be printed to console

```bash
$ gradle bootrun

> Task :bootRun
{"@timestamp":"2020-03-02T09:35:39.737+0700","@team":"bss","@suffix":"applog","thread":"main","correlationId":"","referenceId":"","referenceKey":"","level":"INFO","tags":["demo-cli"],"logger":"t.c.t.s.d.DemoCliappLogStarterApplication","message":"Starting DemoCliappLogStarterApplication on aikano with PID 20416 (/data/workspace/itsd/springboot/demo/demo-cliapp-log-starter/build/classes/java/main started by rookie in /data/workspace/itsd/springboot/demo/demo-cliapp-log-starter)"}
{"@timestamp":"2020-03-02T09:35:39.741+0700","@team":"bss","@suffix":"applog","thread":"main","correlationId":"","referenceId":"","referenceKey":"","level":"INFO","tags":["demo-cli"],"logger":"t.c.t.s.d.DemoCliappLogStarterApplication","message":"No active profile set, falling back to default profiles: default"}
{"@timestamp":"2020-03-02T09:35:40.035+0700","@team":"bss","@suffix":"applog","thread":"main","correlationId":"","referenceId":"","referenceKey":"","level":"INFO","tags":["demo-cli"],"logger":"t.c.t.s.d.DemoCliappLogStarterApplication","message":"Started DemoCliappLogStarterApplication in 0.754 seconds (JVM running for 0.984)"}
{"@timestamp":"2020-03-02T09:35:40.036+0700","@team":"bss","@suffix":"applog","thread":"main","correlationId":"","referenceId":"","referenceKey":"","level":"INFO","tags":["demo-cli"],"logger":"t.c.t.s.d.DemoCliappLogStarterApplication","message":"Hello world"}

BUILD SUCCESSFUL in 1s
3 actionable tasks: 1 executed, 2 up-to-date
```

